export interface PasswordEvent {
  valid: boolean;
  password: string;
}
