import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import { WizardState } from './models/wizard-state';

Vue.config.productionTip = false;

new Vue({
  data: () => {
    const wizard: WizardState = {
      sysKey: '',
      skeletonKey: '',
      storage: '',
      sementeira: [],
      netKey: '',
    };
    return wizard;
  },
  router,
  render: (h) => h(App),
}).$mount('#app');
